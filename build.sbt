name := "todo"

version := "1.0-SNAPSHOT"

scalaVersion := "2.11.6"

libraryDependencies ++= Seq(
  javaJdbc,
  javaJpa.exclude("org.hibernate.javax.persistence", "hibernate-jpa-2.0-api"),
  cache,
  filters,
  javaWs,
  "org.hibernate" % "hibernate-entitymanager" % "4.3.5.Final",
  "com.mysema.querydsl" % "querydsl-jpa" % "3.3.4",
  "postgresql" % "postgresql" % "9.1-901-1.jdbc4",
  "org.mindrot" % "jbcrypt" % "0.3m",
  "commons-io" % "commons-io" % "2.4"
)

com.typesafe.sbt.SbtNativePackager.projectSettings ++ QueryDSLPlugin.queryDSLSettings

// Enable Java Play and QueryDSL
lazy val root = (project in file(".")).enablePlugins(PlayJava).configs(QueryDSLPlugin.QueryDSL)

// Enable Less compilation
includeFilter in (Assets, LessKeys.less) := "*.less"

excludeFilter in (Assets, LessKeys.less) := "_*.less"