# --- !Ups

create table app_user (
  id                        bigint not null,
  email                     varchar(255) not null,
  passwordHash              varchar(255) not null,
  fullName                  varchar(255) not null,
  constraint pk_app_user primary key (id))
;

create table todo_item (
  id                        bigint not null,
  text                      varchar(255) not null,
  reminder                  timestamp,
  done                      boolean,
  list_id                   bigint not null,
  constraint pk_todo_item primary key (id))
;

create table todo_list (
  id                        bigint not null,
  name                      varchar(255) not null,
  owner_id                  bigint not null,
  constraint pk_todo_list primary key (id))
;

create sequence hibernate_sequence;

alter table todo_item add constraint fk_todo_item_list_1 foreign key (list_id) references todo_list (id) on delete restrict on update restrict;
create index ix_todo_item_list_1 on todo_item (list_id);
alter table todo_list add constraint fk_todo_list_owner_2 foreign key (owner_id) references app_user (id) on delete restrict on update restrict;
create index ix_todo_list_owner_2 on todo_list (owner_id);



# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists app_user;

drop table if exists todo_item;

drop table if exists todo_list;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists hibernate_sequence;

