package controllers;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import models.QAppUser;
import play.Application;
import play.db.jpa.JPA;
import play.libs.Yaml;

import com.mysema.query.jpa.impl.JPAQuery;


public class InitialData {
    
    public static void insert(Application app) {
        String fileName = "initial-data.yml";

        EntityManager em = JPA.em("default");
        try {
            em.getTransaction().begin();

            QAppUser user = QAppUser.appUser;
            long count = new JPAQuery(em).from(user).count();

            if(count == 0) {
    
                @SuppressWarnings("unchecked")
                Map<String,List<Object>> all = (Map<String,List<Object>>)Yaml.load(fileName);
    
                saveAll(em, all.get("users"));
                saveAll(em, all.get("lists"));
                saveAll(em, all.get("items"));
            }
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    private static void saveAll(EntityManager em, List<Object> users) {
        for (Object user : users) {
            em.persist(user);
        }
    }
    
}