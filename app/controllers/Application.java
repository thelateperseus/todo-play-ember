package controllers;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import models.AppUser;
import models.Login;
import models.TodoList;
import play.*;
import play.data.Form;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.*;
import views.html.*;

@Transactional
public class Application extends Controller {

    public static Result login() {
        Form<Login> loginForm = Form.form(Login.class).bindFromRequest();
        if (loginForm.hasErrors()) {
            return status(422, loginForm.errorsAsJson());
        } else {
            session().clear();
            String email = loginForm.get().email;
            session("email", email);
            return ok(Json.toJson(AppUser.findByEmail(email)));
        }
    }

    public static Result logout() {
        session().clear();
        return ok("{}");
    }

}
