package controllers;

import java.text.SimpleDateFormat;
import java.util.HashMap;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.ImmutableMap;

import models.TodoItem;
import models.TodoList;
import play.*;
import play.data.Form;
import play.db.jpa.Transactional;
import play.db.jpa.JPA;
import play.libs.Json;
import play.mvc.*;

@Transactional
@Security.Authenticated(Secured.class)
public class TodoItems extends RestController {

    public static Result addItem() throws Exception {
        ObjectNode json = (ObjectNode)request().body().asJson().get("todoItem");
        // Remap list id from EmberData's naming to Play's naming
        json.put("list.id", json.get("list"));
        Form<TodoItem> form = Form.form(TodoItem.class).bind(
            json, "text", "reminder", "list.id");
        if ( form.hasErrors() ) {
            return errorResponse(form);
        } else {
            TodoItem item = form.get();
            TodoList list = TodoList.findList(CurrentUser.current(), item.getList().getId());
            if (list == null) {
                return status(422, jsonWrap("errors",
                    new ImmutableMap.Builder<String,Object>().put("list", "Invalid value").build()));
            }
            JPA.em().persist(item);
            response().setHeader("Location", routes.TodoItems.viewItem(item.getId()).url());
            return created(jsonWrap("todoItem", item));
        }
    }

    public static Result viewItem(Long itemId) {
        TodoItem item = TodoItem.findItem(CurrentUser.current(), itemId);
        if (item == null) {
            return notFound();
        }
        return ok(jsonWrap("todoItem", item));
    }

    public static Result editItem(Long itemId) {
        TodoItem item = TodoItem.findItem(CurrentUser.current(), itemId);
        if (item == null) {
            return notFound();
        }
        TodoList list = item.getList();
        Form<TodoItem> form = Form.form(TodoItem.class).bind(
            request().body().asJson().get("todoItem"), "text", "reminder");
        if ( form.hasErrors() ) {
            return errorResponse(form);
        } else {
            TodoItem updatedItem = form.get();
            updatedItem.setList(list);
            updatedItem.setDone(item.isDone());
            updatedItem.setId(itemId);
            JPA.em().merge(updatedItem);
            return ok(jsonWrap("todoItem", item));
        }
    }

    public static Result updateDone(Long itemId) {
        TodoItem item = TodoItem.findItem(CurrentUser.current(), itemId);
        if (item == null) {
            return notFound();
        }
        ObjectNode todoItemNode = (ObjectNode)request().body().asJson().get("todoItem");
        if ( todoItemNode == null || todoItemNode.get("done") == null ) {
            return status(422, jsonWrap("errors",
                ImmutableMap.<String,String>builder().put("done", "This field is required").build()));
        } else {
            item.setDone(todoItemNode.get("done").booleanValue());
            return ok(jsonWrap("todoItem", item));
        }
    }

    public static Result deleteItem(Long itemId) {
        TodoItem item = TodoItem.findItem(CurrentUser.current(), itemId);
        if (item == null) {
            return notFound();
        }
        JPA.em().remove(item);
        return ok(Json.toJson(new HashMap<String,Object>()));
    }

}
