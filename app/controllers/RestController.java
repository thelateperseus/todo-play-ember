package controllers;

import models.TodoList;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.ImmutableMap;

import play.data.Form;
import play.libs.Json;
import play.mvc.Controller;

public abstract class RestController extends Controller {

    protected static JsonNode jsonWrap(String name, Object object) {
        return Json.toJson(new ImmutableMap.Builder<String,Object>().put(name, object).build());
    }

    protected static <T extends Object> Status errorResponse(Form<T> form) {
        ObjectNode root = Json.newObject();
        root.set("errors", form.errorsAsJson());
        return status(422, root);
    }

    public RestController() {
        super();
    }

}