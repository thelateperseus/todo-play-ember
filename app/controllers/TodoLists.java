package controllers;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;

import models.JsonViews.SummaryView;
import models.TodoItem;
import models.TodoList;
import play.*;
import play.data.Form;
import play.db.jpa.Transactional;
import play.db.jpa.JPA;
import play.libs.Json;
import play.mvc.*;
import play.twirl.api.Content;

@Transactional
@Security.Authenticated(Secured.class)
public class TodoLists extends RestController {

    public static Result addList() {
        JsonNode json = request().body().asJson();
        Form<TodoList> form = Form.form(TodoList.class).bind(json.get("todoList"), "name");
        if ( form.hasErrors() ) {
            return errorResponse(form);
        } else {
            TodoList list = form.get();
            list.setOwner(CurrentUser.current());
            JPA.em().persist(list);
            response().setHeader("Location", routes.TodoLists.viewList(list.getId()).url());
            return created(jsonWrap("todoList", list));
        }
    }

    public static Result viewLists() throws Exception {
        List<TodoList> lists = TodoList.findAllByUser(CurrentUser.current());
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(MapperFeature.DEFAULT_VIEW_INCLUSION);
        String result = mapper.writerWithView(SummaryView.class).writeValueAsString(
           jsonWrap("todoLists", lists));
        return ok(result);
    }

    public static Result viewList(Long id) {
        TodoList list = TodoList.findList(CurrentUser.current(), id);
        if (list == null) {
            return notFound();
        }
        List<TodoItem> items = list.getItems();
        return ok(Json.toJson(
            new ImmutableMap.Builder<String,Object>()
                .put("todoList", list).put("todoItems", items).build()));
    }

    public static Result updateList(Long id) {
        TodoList originalList = TodoList.findList(CurrentUser.current(), id);
        if (originalList == null) {
            return notFound();
        }
        JsonNode json = request().body().asJson();
        Form<TodoList> form = Form.form(TodoList.class).bind(json.get("todoList"), "name");
        if ( form.hasErrors() ) {
            return errorResponse(form);
        } else {
            TodoList formList = form.get();
            /* I wish Play had a populateModel equivalent here so we could just set
             * the fields in the form */
            originalList.setName(formList.getName());
            return ok(jsonWrap("todoList", originalList));
        }
    }

}
