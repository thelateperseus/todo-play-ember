package models;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import play.data.format.Formats;
import play.data.validation.Constraints;
import play.data.validation.ValidationError;
import play.db.jpa.JPA;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.google.common.collect.ImmutableMap;
import com.mysema.query.jpa.impl.JPAQuery;

import controllers.CurrentUser;

@Entity
@Table(name="todo_item")
public class TodoItem {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    @Constraints.Required
    @Column(nullable = false)
    private String text;
    
    @Formats.DateTime(pattern="yyyy-MM-dd'T'HH:mm:ss.SSSX")
    private Date reminder;
    
    private boolean done = false;

    @JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
    @JsonIdentityReference(alwaysAsId=true)
    @JoinColumn(nullable=false)
    @ManyToOne(fetch=FetchType.EAGER, optional=false)
    private TodoList list;

    // -- Getter/Setter

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getReminder() {
        return reminder;
    }

    public void setReminder(Date reminder) {
        this.reminder = reminder;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public TodoList getList() {
        return list;
    }

    public void setList(TodoList list) {
        this.list = list;
    }

    // -- Queries

    public static TodoItem findItem(AppUser owner, Long itemId) {
        QTodoItem item = QTodoItem.todoItem;
        QTodoList list = QTodoList.todoList;
        QAppUser user = QAppUser.appUser;
        return new JPAQuery(JPA.em())
            .from(item)
            .innerJoin(item.list, list)
            .innerJoin(list.owner, user)
            .where(item.id.eq(itemId).and(user.eq(owner)))
            .uniqueResult(item);
    }
}
