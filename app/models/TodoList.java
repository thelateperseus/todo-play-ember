package models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;

import models.JsonViews.SummaryView;
import play.data.validation.Constraints;
import play.db.jpa.JPA;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.mysema.query.jpa.impl.JPAQuery;

@Entity
@Table(name="todo_list")
public class TodoList {   
    @JsonView(SummaryView.class)
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    @JsonView(SummaryView.class)
    @Constraints.Required
    @Column(nullable = false)
    private String name;

    @JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
    @JsonIdentityReference(alwaysAsId=true)
    @OneToMany(mappedBy="list", fetch=FetchType.LAZY, orphanRemoval=true, cascade=CascadeType.ALL)
    @OrderBy(value="text")
    private List<TodoItem> items;

    @JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
    @JsonIdentityReference(alwaysAsId=true)
    @JoinColumn(nullable=false)
    @ManyToOne(fetch=FetchType.EAGER, optional=false)
    private AppUser owner;

    // -- Getter/Setter

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<TodoItem> getItems() {
        return items;
    }

    public void setItems(List<TodoItem> items) {
        this.items = items;
    }

    public AppUser getOwner() {
        return owner;
    }

    public void setOwner(AppUser owner) {
        this.owner = owner;
    }

    // -- Queries

    public static List<TodoList> findAllByUser(AppUser owner) {
        QTodoList list = QTodoList.todoList;
        QAppUser user = QAppUser.appUser;
        return new JPAQuery(JPA.em())
            .from(list)
            .innerJoin(list.owner, user)
            .where(user.eq(owner))
            .orderBy(list.name.asc())
            .list(list);
    }

    public static TodoList findList(AppUser owner, Long listId) {
        QTodoList list = QTodoList.todoList;
        QAppUser user = QAppUser.appUser;
        return new JPAQuery(JPA.em())
            .from(list)
            .innerJoin(list.owner, user)
            .where(user.eq(owner).and(list.id.eq(listId)))
            .uniqueResult(list);
    }

}
