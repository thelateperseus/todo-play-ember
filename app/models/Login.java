package models;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import play.data.validation.Constraints;
import play.data.validation.ValidationError;

public class Login {
    
    @Constraints.Required
    public String email;

    @Constraints.Required
    public String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Map<String, List<ValidationError>> validate() {
        if (AppUser.authenticate(email, password) == null) {
          return fieldError("email", "Invalid user or password");
        }
        return null;
    }

    public static Map<String, List<ValidationError>> fieldError(String fieldName, String error) {
        Map<String, List<ValidationError>> errors = new HashMap<>();
        errors.put(fieldName, Arrays.asList(new ValidationError(fieldName, error)));
        return errors;
    }
}
