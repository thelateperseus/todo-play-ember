package models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.mindrot.jbcrypt.BCrypt;

import play.data.validation.Constraints;
import play.db.jpa.JPA;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mysema.query.jpa.impl.JPAQuery;

@Entity
@Table(name="app_user")
public class AppUser {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    @Constraints.Required
    @Column(nullable=false)
    private String email;

    @JsonIgnore
    @Constraints.Required
    @Column(nullable=false)
    private String passwordHash;

    @Constraints.Required
    @Column(nullable=false)
    private String fullName;

    @JsonIgnore
    @OneToMany(mappedBy="owner", fetch=FetchType.LAZY, orphanRemoval=true, cascade=CascadeType.ALL)
    @OrderBy(value="id")
    private List<TodoList> lists;

    // -- Getter/Setter
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public List<TodoList> getLists() {
        return lists;
    }

    public void setLists(List<TodoList> lists) {
        this.lists = lists;
    }

    // -- Queries

    /**
     * Retrieve a User from email.
     */
    public static AppUser findByEmail(String email) {
        if (email == null) {
            return null;
        } else {
            QAppUser user = QAppUser.appUser;
            return new JPAQuery(JPA.em())
                .from(user)
                .where(user.email.eq(email))
                .uniqueResult(user);
        }
    }
    
    /**
     * Authenticate a User.
     */
    public static AppUser authenticate(String email, String password) {
        AppUser user = findByEmail(email);
        if (user == null) {
            return null;
        } else if (BCrypt.checkpw(password, user.passwordHash)) {
            return user;
        } else {
            return null;
        }
    }
}
