import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;

import org.apache.commons.io.IOUtils;

import play.Application;
import play.GlobalSettings;
import play.Play;
import play.api.mvc.EssentialFilter;
import play.filters.csrf.CSRFFilter;
import play.libs.F.Promise;
import play.mvc.Action;
import play.mvc.Result;
import play.mvc.Http.Request;
import play.mvc.Http.RequestHeader;
import play.mvc.Results;
import controllers.CurrentUser;
import controllers.InitialData;


public class Global extends GlobalSettings {
    @Override
    public Action onRequest(Request request, Method actionMethod) {
        return new CurrentUser();
    }
    
    @Override
    public void onStart(Application app) {
        InitialData.insert(app);
    }

    @Override
    public <T extends EssentialFilter> Class<T>[] filters() {
        return new Class[]{CSRFFilter.class};
    }

    @Override
    public Promise<Result> onHandlerNotFound(RequestHeader header) {
        try (InputStream in = this.getClass().getResourceAsStream("/public/index.html")) {
            return Promise.pure((Result)Results.ok(IOUtils.toString(in)).as("text/html"));
        } catch (IOException e) {
            throw new RuntimeException(e.toString(), e);
        }
    }
}
