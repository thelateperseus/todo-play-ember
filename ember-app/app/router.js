import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

export default Router.map(function() {
    this.route('login');
    this.resource('todoLists', function(){
        this.resource('todoList', { path:'/:id' });
        this.route('create');
    });
});
