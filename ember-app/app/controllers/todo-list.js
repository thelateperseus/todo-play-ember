import Ember from 'ember';

export default Ember.Controller.extend({
    actions: {
        editList: function(){
            this.set('isEditing', true);
        },
        acceptChanges: function() {
            this.set('isEditing', false);
            this.get('model').save();
        },
    },

    isEditing: false,
});
