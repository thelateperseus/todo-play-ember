import Ember from 'ember';
/* global moment */

export default Ember.ArrayController.extend({
    needs: 'todo-list',
    actions: {
        addItem: function(){
            this.set('isAddingItem', true);
        },
        cancelAddItem: function() {
            this.set('isAddingItem', false);
        },
        saveNewItem: function() {
            this.set('isAddingItem', false);
            var text = this.get('newText');
            if ( !text.trim() ) { 
                return; 
            }
            this.set('newText', '');

            var reminderText = this.get('newReminder');
            var item = this.store.createRecord('todo-item', {
                list: this.parentController.model,
                text: text,
                reminder: reminderText == null ? null : moment(reminderText, 'ddd, MMM D YYYY').toDate(),
                done: false
            });
            item.save();            
        },
    },

    isAddingItem: false,

    sortProperties: ['text'],
    sortAscending: true
});
