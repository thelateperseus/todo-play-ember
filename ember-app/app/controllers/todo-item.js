import Ember from 'ember';
/* global bootbox */
/* global $ */

export default Ember.Controller.extend({
    isDone: function(key, value) {
        var model = this.get('model');

        if (value === undefined) {
            // property being used as a getter
            return model.get('done');
        } else {
            // property being used as a setter
            model.set('done', value);
            $.ajax({
                url: '/api/v1/todoItems/' + model.get('id'),
                method: 'PATCH',
                data: JSON.stringify( { todoItem : { done : model.get('done') } } ),
                dataType: 'json',
                contentType: 'application/json'
            }).fail(function(jqXHR, textStatus, errorThrown) {
                // TODO Handle errors
                console.log('Error setting done for item ' + model.get('id') + ' ' + 
                    textStatus + ' ' + errorThrown);
            });            
            return value;
        }
    }.property('model.done'),

    actions: {
        removeItem: function(){
            var item = this.get('model');
            bootbox.confirm('Are you sure want to delete?', function(result) {
                if (result) { 
                    item.deleteRecord();
                    item.save();
                }
            });
        },
        editItem: function(){
            this.set('isEditingItem', true);
        },
        cancelEditItem: function() {
            this.set('isEditingItem', false);
        },
        saveItem: function() {
            this.set('isEditingItem', false);
            var item = this.get('model');
            item.save();            
        },
    },

    isEditingItem: false,
});
