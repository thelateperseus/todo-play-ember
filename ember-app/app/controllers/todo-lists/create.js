import Ember from 'ember';

export default Ember.Controller.extend({
    actions: {
        save: function(){
            var newList = this.get('model');
            newList.save().then(function() {
                this.transitionToRoute('todoList', newList);
            });
        }
    }
});