import Ember from 'ember';
import Resolver from 'ember/resolver';
import loadInitializers from 'ember/load-initializers';
import config from './config/environment';
import DS from 'ember-data';

var App;

Ember.MODEL_FACTORY_INJECTIONS = true;

App = Ember.Application.extend({
    modulePrefix: config.modulePrefix,
    podModulePrefix: config.podModulePrefix,
    Resolver: Resolver,
    LOG_TRANSITIONS: true,
    LOG_TRANSITIONS_INTERNAL: true,
    LOG_VIEW_LOOKUPS: true
});

DS.RESTAdapter.reopen({
    namespace: 'api/v1',

    ajaxError: function(jqXHR) {
        this._super(jqXHR);

        var response = Ember.$.parseJSON(jqXHR.responseText);
        if (response.errors) {
            return new DS.InvalidError(response.errors);
        } else {
            return new DS.InvalidError({ summary: 'Error connecting to the server.' });
        }
    }
});

loadInitializers(App, config.modulePrefix);

export default App;
