import Ember from 'ember';
import AuthenticatedRouteMixin from 'simple-auth/mixins/authenticated-route-mixin';

export default Ember.Route.extend(AuthenticatedRouteMixin, {
    model: function(params) {
        return this.store.fetchById('todo-list', params.id);
    }
    /* Seems to not be required now, but it used to be?
    setupController: function( controller, model ) {
        controller.set( 'model', this.store.fetchById('todo-list', model.id) );
    }*/
});
