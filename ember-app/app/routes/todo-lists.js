import Ember from 'ember';
import AuthenticatedRouteMixin from 'simple-auth/mixins/authenticated-route-mixin';

export default Ember.Route.extend(AuthenticatedRouteMixin, {
    model: function () {
        this.store.find('todo-list');
        return this.store.filter('todo-list',function(todoList){
            return !todoList.get('isNew');
        });
    }
});
