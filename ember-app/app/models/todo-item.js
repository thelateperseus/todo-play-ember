import DS from 'ember-data';

// TodoList
export default DS.Model.extend({
    text: DS.attr('string'),
    reminder: DS.attr('date'),
    done: DS.attr('boolean'),
    list: DS.belongsTo('todoList')
});
