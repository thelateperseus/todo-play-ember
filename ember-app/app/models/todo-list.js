import DS from 'ember-data';

// TodoList
export default DS.Model.extend({
    name: DS.attr('string'),
    items: DS.hasMany('todo-item', { async: true })
});
