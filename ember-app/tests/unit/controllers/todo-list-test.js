import Ember from 'ember';
import startApp from '../../helpers/start-app';
import {
  moduleFor,
  test
} from 'ember-qunit';

var App;
var store;

moduleFor('controller:todo-list', {
    needs: ['controller:application'],
 
    setup: function () {
        App = startApp();
        store = App.__container__.lookup('store:main');
    },
 
    teardown: function () {
        Ember.run(App, App.destroy);
    }
});

test('calling the action editList sets isEditing to true', function(assert) {
    // get the controller instance
    var ctrl = this.subject();

    // check the properties before the action is triggered
    assert.equal(ctrl.get('isEditing'), false);

    // trigger the action on the controller by using the `send` method,
    // passing in any params that our action may be expecting
    ctrl.send('editList');

    // finally we assert that our values have been updated
    // by triggering our action.
    assert.equal(ctrl.get('isEditing'), true);
});

test('calling the action acceptChanges saves the model and sets isEditing to false', function(assert) {
    // The expect is crucial here, otherwise we can get false positives.
    assert.expect(4);
    
    var model = null;
    store.scheduleSave = function(context, resolver) {
        assert.ok(resolver.promise); // The actual assert
        resolver.resolve(context);
    };

    Ember.run(function () {
        model = store.createRecord('todo-list');
    });
    var ctrl = this.subject({
        store: store,
        model: model,
        isEditing: true
    });

    assert.equal(ctrl.get('model'), model, 'initial model');
    assert.equal(ctrl.get('isEditing'), true, 'initial isEditing');

    ctrl.send('acceptChanges');

    assert.equal(ctrl.get('isEditing'), false, 'final isEditing');
});
